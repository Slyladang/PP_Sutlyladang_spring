package com.ladang.demo.services;


import com.ladang.demo.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Integer count();
}
