package com.ladang.demo.services;


import com.ladang.demo.models.Book;

import java.util.List;

public interface BookService {

    List<Book> getAll();

    Book findOne(Integer id);

    boolean update(Book book);

    boolean remove(Integer id);

    Integer count();

    boolean create(Book book);
}
