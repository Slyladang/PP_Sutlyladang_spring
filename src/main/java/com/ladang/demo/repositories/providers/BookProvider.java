package com.ladang.demo.repositories.providers;


import com.ladang.demo.models.Book;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

        public String getAllProvider(){
            return new SQL(){{
                SELECT("*");
                FROM("tb_book b");
                INNER_JOIN("tb_category c ON b.cate_id = c.id");
                ORDER_BY("b.id desc");
            }}.toString();
        }


        public String create(Book book){
            return new SQL(){{
                INSERT_INTO("tb_book");
                VALUES("title", "#{title}");
                VALUES("author", "#{author}");
                VALUES("publisher", "#{publisher}");
                VALUES("thumbnail", "#{thumbnail}");
                VALUES("cate_id" ,"#{category.id}");

            }}.toString();
        }



}
