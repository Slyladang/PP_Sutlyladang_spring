package com.ladang.demo.controllers;


import com.ladang.demo.models.Book;
import com.ladang.demo.models.Category;
import com.ladang.demo.services.BookService;
import com.ladang.demo.services.CategoryService;
import com.ladang.demo.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Controller
public class BookManagementController {

    private BookService bookService;

    @Autowired
    private CategoryService categoryService;

    private UploadService uploadService;

    public BookManagementController(BookService bookService, UploadService uploadService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
    }

        @GetMapping("/")
    public String allBook(ModelMap model) {

        List<Book> bookList = this.bookService.getAll();

        model.addAttribute("books", bookList);
            System.out.println(bookList);
        return "all-book";
    }

    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id") Integer id, Model model) {

        System.out.println("ID: " + id);

        Book book = this.bookService.findOne(id);

        model.addAttribute("book", book);

        return "book/view-detail";
    }


    @GetMapping("/update/{id}")
    public String showUpdateForm(@PathVariable Integer id, ModelMap modelMap) {

        Book book = this.bookService.findOne(id);

        modelMap.addAttribute("isNew", false);
        modelMap.addAttribute("book", book);

        List<Category> categories = this.categoryService.getAll();
        modelMap.addAttribute("categories", categories);

        return "book/add-book";
    }

    @GetMapping("/create")
    public String create(ModelMap modelMap) {
        modelMap.addAttribute("isNew", true);
        modelMap.addAttribute("book", new Book());

        List<Category> categories = this.categoryService.getAll();
        modelMap.addAttribute("categories", categories);

        return "book/add-book";
    }


    @PostMapping("update/submit")
    public String updateSubmit(@ModelAttribute Book book, MultipartFile file) {
        System.out.println(book);


        String filename = this.uploadService.upload(file);

        book.setThumbnail(filename);

        this.bookService.update(book);

        return "redirect:/";
    }


    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.bookService.remove(id);

        return "redirect:/";
    }



    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file, Model model) {
        System.out.println(book);

        if (bindingResult.hasErrors()) {
            model.addAttribute("isNew", true);

            List<Category> categories = this.categoryService.getAll();
            model.addAttribute("categories", categories);

            return "redirect:/";
        }

        String filename = this.uploadService.upload(file, "pp/");


        book.setThumbnail(filename);


        this.bookService.create(book);

        return "redirect:/book";
    }





}
